#!/bin/bash

# Dynamically get start/end dates for last month. The syntax works for BSD systems, so may have to be changed for Linux
#START=`date -v -1m '+%Y-%m'`-01
#END=`date '+%Y-%m'`-01

# Manually set START and END date

START='2018-01-01'
END='2018-02-01'

#print start and end dates
#echo $START
#echo $END


aws ce get-cost-and-usage \
--region us-east-1 \
--time-period Start=$START,End=$END \
--granularity MONTHLY \
--metrics UnblendedCost \
--group-by Type=DIMENSION,Key=LINKED_ACCOUNT
